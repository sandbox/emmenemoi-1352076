<?php

/**
 * Implementation of cck hook_field_settings() for payments field
 */
function _hipay_field_payments_field_settings($op, $field) {

  if (_hipay_field_api_loadandwarn() === false)
		return;
		
   switch ($op) {
      case 'database columns':
         $columns['value'] = array('type' => 'int', 'size' => 'tiny','not null' => FALSE, 'sortable' => FALSE, 'views' => TRUE);
         return $columns;
       case 'form':
         $form = array();
         $all_fields =  content_fields();
         $cck_field_list = array();
         foreach($all_fields as $field_name=>$field_data) {
            $cck_field_list[$field_data['type_name'].'---'.$field_name] = $field_name.' on '.$field_data['type_name'].': '.$field_data['widget']['label'];
         }
         $form['price_field'] = array(
           '#type' => 'select',
           '#title' => t('Price field'),
           '#default_value' => $field['price_field'],
           '#options' => $cck_field_list,
           '#description' => t('The field where the price will be grabbed.'),
           '#required' => TRUE,
         );

         $form['hide_field'] = array(
           '#type' => 'select',
           '#title' => t('Optional field to hide'),
           '#default_value' => $field['hide_field'],
           '#options' => array('-'=>'') + $cck_field_list,
           '#description' => t('This field will be hidden if purchase button is presented to the user.'),
         );
         
         $form['affiliates'] = array(
           '#type' => 'fieldset',
           '#title' => t('Affiliates'),
           '#description' => '<p>'.t('Please chose here the properties that should be applied to each affiliates.').'</p>' ,
           '#tree'=>true,
           '#prefix' => '<div id="affiliates-wrapper">',
           '#suffix'=>'</div>', 
           '#weight'=>1,
         );
        /* 
        $form['affiliates_sub']['add'] = array(
          '#type' => 'button',
          '#value' => t('Add new affiliate'),
          '#weight'   => 0,
          '#ahah' => array(
            'event' => 'clicked', // When the button is "clicked", AHAH will do it's job
            'path' => ahah_helper_path(array('affiliates_sub','affiliates')), // The array features the wrapper form field. So our form wrapper is $form['cars'], so we set this to array('cars'). If your form was $form['cars']['another_wrapper'], the path would be array('cars', 'another_wrapper').
            'wrapper' => 'affiliates-wrapper', // We then define the wrapper which will be changed.
          ),
        );*/
        //dsm($field);
        //$field_value['affiliates'] = (array) unserialize($field['affiliates']) + array(0=>array('tx'=>NULL, 'account_field'=>NULL));
        
        //$form['affiliates'] += array('#prefix' => '<div id="affiliates-wrapper">', '#suffix'=>'</div>', '#weight'=>1, '#tree' => TRUE);

        $form['affiliates'][0] = array('#prefix' => '<div id="inline-form">', '#suffix'=>'</div>', '#weight'=>1, '#tree' => TRUE);
        $form['affiliates'][0]['tx'] = array(  
              '#title' => t('Rate in %'),
              '#type' => 'textfield',
              '#size' => 5,
              '#maxlength' => 5,
              '#default_value' => $field['affiliates'][0]['tx'],
              );
        
        $form['affiliates'][0]['account_field'] = array(  
              '#title' => t('Account field'),
              '#type' => 'select',
              '#options' => $cck_field_list,
              '#default_value' => $field['affiliates'][0]['account_field'],
              );
              
         $form['site_costs'] = array(
           '#title' => t('Global commission rate in %'),
           '#type' => 'textfield',
           '#size' => 5,
           '#maxlength' => 5,
           '#default_value' => $field['site_costs'],
           '#description' => t('This amount will be added to cover the transaction costs.'),
           '#required' => TRUE,
         );
         $form['site_fixed_costs'] = array(
           '#value' => t('Additional fixed costs: ').HIPAY_FIELD_FIXED_FEES
         );
         
         $capture_option = array(HIPAY_MAPI_CAPTURE_IMMEDIATE=>t('Capture done asap'), HIPAY_MAPI_CAPTURE_MANUAL=>'Manual capture');
         for ($i=1; $i<=HIPAY_MAPI_CAPTURE_MAX_DAYS ; $i++) {
            $capture_option[$i] = t('After @count days.',array('@count'=>$i));
         }
         $form['capture_day'] = array(
           '#type' => 'select',
           '#title' => t('Capture day'),
           '#default_value' => $field['capture_day'],
           '#options' => $capture_option,
           '#description' => t('How many time to wait between the authorisation and final payment capture.'),
           '#required' => TRUE,
         );
   
         $rating_option = array('ALL'=>t('Available to any public'));
         foreach (array(12,16,18) as $value ) {
            $rating_option['+'.$value] = t('Only to a public over @age.',array('@age'=>$value));
         }
         $form['rating'] = array(
           '#type' => 'select',
           '#title' => t('Order rating'),
           '#default_value' => $field['rating'],
           '#options' => $rating_option,
           '#description' => t('Public rating'),
           '#required' => TRUE,
         );
   
         $form['affiliates_type'] = array(
           '#type' => 'radios',
           '#title' => t('Affiliates Type'),
           '#description' => t('Consider affiliates given price to include (private) or exclude (professional) VAT.'),
           '#default_value' => !empty($field['affiliates_type']) ? $field['affiliates_type'] : 0,
           '#options' => array(0 => 'Private', 1 => 'Professional'),
           '#required' => FALSE,
         );
         
         $form['grabb_all'] = array(
           '#type' => 'checkbox',
           '#title' => t('Grabb all amount if no affiliate? (deactivate hipay payment if unchecked)'),
           '#default_value' => is_numeric($field['grabb_all']) ? $field['grabb_all'] : 1 ,
         );
         
         $form['shippable'] = array(
           '#type' => 'checkbox',
           '#title' => t('Is this product shippable? (will hide requests for shipping informations)'),
           '#default_value' => is_numeric($field['shippable']) ? $field['shippable'] : 1 ,
         );
         $text_cgu = module_exists('i18nstrings') ? i18nstrings('cck:field:'. $field['field_name'] .':widget_cgu',$field['cgu']) : $field['cgu'];
         $form['cgu'] = array(
                 '#type' => 'textarea',
                 '#cols' => 20,
                 '#default_value' => $text_cgu,
                 '#title' => t('Terms of use of the field'),
                 '#description' => '<p>'.t('You can add terms of use to be accepted. Leave blank if no terms of use.').'</p>' ,
                 '#weight'=>8,
            );
         
         $form['multiple'] = array(
           '#type' => 'hidden',
           '#value' => '0',
         );
         $form['required'] = array(
           '#type' => 'hidden',
           '#value' => NULL,
         ); 
         return $form;
       case 'validate':
         if( module_exists('i18nstrings'))
            i18nstrings_update('cck:field:'. $field['field_name'] .':widget_cgu',$field['cgu']);
         break;
       case 'save':
         return array('price_field', 'affiliates', 'site_costs', 'capture_day', 'rating', 'affiliates_type', 'grabb_all', 'hide_field', 'shippable', 'cgu');
     }
   
}


/**
 * Implementation of hook_field().
 */
function _hipay_field_payments_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'validate':
/*      $allowed_values = content_allowed_values($field);
      if (is_array($items)) {
        foreach ($items as $delta => $item) {
          $error_element = isset($item['_error_element']) ? $item['_error_element'] : '';
          if (is_array($item) && isset($item['_error_element'])) unset($item['_error_element']);
          if (!empty($item['value'])) {
            if (count($allowed_values) && !array_key_exists($item['value'], $allowed_values)) {
              form_set_error($error_element, t('%name: illegal value.', array('%name' => t($field['widget']['label']))));
            }
            if (!empty($field['max_length']) && drupal_strlen($item['value']) > $field['max_length']) {
              form_set_error($error_element, t('%name: the value may not be longer than %max characters.', array('%name' => $field['widget']['label'], '%max' => $field['max_length'])));
            }
          }
        }
      }
      return $items;
*/
        break;
    case 'sanitize':
      foreach ($items as $delta => $item) {
        $text = isset($item['value']) ? check_plain($item['value']) : '';
        $items[$delta]['safe'] = $text;
      }
  }
}


/**
 * Theme function for 'default' text field formatter.
 */
function theme_hipay_field_formatter_default($element, $popup=false, $data=null) {
  if (!user_access('view payment button'))
    return false;
  $field = content_fields($element['#field_name']);
  
  $data_attr = array();
  if (empty($data)) {
      $data = $field['widget']['label'];
      $class='css_button';
  } else {
      $data_attr = array('html'=>true);
  }

  if ($popup) {
     $popup_attr = array('rel'=>'lightframe[|width:660px; height:700px; scrolling: auto;]');
  } else {
     $popup_attr = array();
  }
  $node = $element['#node'];

  // For "some" backwards compatibility
  $node_field_item['value'] = $element['#item']['value'];

  // Allow the value to be formated from hipay_field_hook_display
  $display_func = 'hipay_field_'. $field['field_name'] .'_display';
  if (function_exists($display_func)) {
    $display = $display_func($field, $element, $node);
  } else {
     $hipay_data = _hipay_field_get_payment_info($node, $element['#field_name']);
     //dsm(hipay_field_get_affiliate_account($owner));
     if (is_array($hipay_data) && !hipay_field_content_is_empty(hipay_field_get_affiliate_account($hipay_data['affiliates'][0]), $field)) {
        $sandbox = variable_get(HIPAY_FIELD_SANDBOX_VAR, TRUE);
        _hipay_field_i8n_addcss();
        $display = l($data,'hipay_field/payment/'.$node->nid.'/'.$element['#field_name'],array('attributes'=> (array('class'=>$class)+$popup_attr))+$data_attr);
        //$display = "<button onClick='".url('hipay_field/payment/'.$node->nid.'/'.$element['#field_name'],array('attributes'=>array('rel'=>'lightframe[|width:660px; height:700px; scrolling: auto;]')))."'>";
        //$display= "<input type='button' value='BUY FOR ".$hipay_data['gross_price']." ".variable_get(_hipay_field_get_variable(HIPAY_FIELD_CURRENCY_VAR,$sandbox), 'EUR')."' class='".$field['module']." ".$field['module']."-".$field['type']."'>";
     }
  }
  
  return $display;
}


/**
 * Theme function for 'personalized' text field formatter.
 */
function theme_hipay_field_formatter_popup($element) {
    return theme_hipay_field_formatter_default($element, true);
}

/**
 * Theme function for 'hipay' text field formatter.
 */
function theme_hipay_field_formatter_hipay($element) {
  $base_path = url(drupal_get_path('module','hipay_field'));
  $base_image = $base_path.'/misc/transparent.png';
  $data = '<img src="'.$base_image.'" class="hipay_field_button_buy hipay_field_sprites">';
  return theme_hipay_field_formatter_default($element, false, $data);
}

function _hipay_field_payments_widget(&$form, &$form_state, $field, $items, $delta = 0) {
   return array();//text_widget($form, $form_state, $field, $items, $delta);
}

function _hipay_field_payments_content_is_empty($item, $field) {
  //dsm($field);
  //echo "<!-- HIPAY ".print_r($item,true)." -->";
  if (empty($item['account']) && empty($item['userid']) && $field['grabb_all']!=1 ) {
    return TRUE;
  }
  return FALSE;
}

// Page to present the payment (including a message)
function hipay_field_pay($node, $field_name) {
   $hipay_data = _hipay_field_get_payment_info($node,$field_name); 
   //print drupal_get_form('hipay_field_pay_form',array('hipay_data'=>$hipay_data));
   //dsm($hipay_data);
   if ($hipay_data['field']['widget']['type']=='hipay_widget')
        print theme('page',drupal_get_form('hipay_field_pay_form',array('hipay_data'=>$hipay_data)), $hipay_data);
   else
        print theme('hipay_field_payment',drupal_get_form('hipay_field_pay_form',array('hipay_data'=>$hipay_data)), $hipay_data);
   exit; 
}

function hipay_field_pay_form($form_state, $args) {   
   $hipay_data = empty($form_state['hipay_data']) ? $args['hipay_data'] : $form_state['hipay_data'];
   global $user;
   //dsm($hipay_data);
   //print_r($form_state);
   
   drupal_add_css(drupal_get_path('module', 'hipay_field').'/hipay_field.css');
   $infos =  '<div id="hipay_field_order_confirm">';
   $infos .=  t("<h3>Secured Credit Card purchase via Hipay payment system</h3>
        <ul>
            <li>Following the capture of your payment information, your purchase will be on hold.</li>
            <li>You will be debited as soon as the seller will confirm the order.</li>
            <li>If this purchase is not confirmed within !capture_days days, the payment will be automatically cancelled and you won't be debited.</li>
        </ul>", array('!capture_days'=>_hipay_confirmation_delay($hipay_data['field'])));
   
   $form['#prefix'] = $infos;
   $form['#suffix'] = "</div>";
   
   $form['group'] = array(
        '#type' => 'fieldset',
        '#title' => t("Purchase on !user's shop", array('!node_title'=>$hipay_data['node']->title,'!user'=>$hipay_data['affiliates'][0]->name)),
        '#description' => '' ,
        '#weight'=>1,
   );
   
   $price = $hipay_data['net_price'];  
   $total_price =  $hipay_data['gross_price'];
   $tax_value=variable_get(HIPAY_FIELD_TAX_VALUE_VAR, 19.6);
   // si affilié privé: on applique sur le cout
   if (!_hipay_field_affiliates_content_is_empty( hipay_field_get_affiliate_account($hipay_data['affiliates'][0]), $hipay_data['field']) && $hipay_data['field']['affiliates_type']==0){
      $costs = $hipay_data['costs'];
      $tax = variable_get(_hipay_field_get_variable(HIPAY_FIELD_TAX_ACCOUNT_VAR,$sandbox), '') != '' ? $costs*$tax_value/100: '';
   // si affilié pro: on ajouste le montant HT et on applique sur le total
   } elseif (!_hipay_field_affiliates_content_is_empty( hipay_field_get_affiliate_account($hipay_data['affiliates'][0]), $hipay_data['field']) && $hipay_data['field']['affiliates_type']==1){
      $costs = ($total_price-$price);
      $price = $price/($tax_value+100)*100;
      $tax = variable_get(_hipay_field_get_variable(HIPAY_FIELD_TAX_ACCOUNT_VAR,$sandbox), '') != '' ? $total_price*$tax_value/(100+$tax_value): '';
   } else {
      $total_price = $price ;
      $tax = variable_get(_hipay_field_get_variable(HIPAY_FIELD_TAX_ACCOUNT_VAR,$sandbox), '') != '' ? $total_price*$tax_value/(100+$tax_value): '';
      $price = $total_price - $tax;
   }
   $currency = variable_get(_hipay_field_get_variable(HIPAY_FIELD_CURRENCY_VAR,$sandbox), 'EUR') ;

   $price = number_format($price, 2, ',', ' ');
   $total_price = number_format($total_price, 2, ',', ' ');
   $tax = number_format($tax, 2, ',', ' ');
   $costs = isset($costs) ? number_format($costs, 2, ',', ' ') : null;
   //$costs = isset($costs) ? number_format($hipay_data['costs'], 2, ',', ' ') : null;
   //$costs = $hipay_data['costs'];

   $payment_summary = "<table class='payment_summary'><tr class='product'><td>{$hipay_data['node']->title}</td><td>$price</td></tr>";
   if (isset($costs))
       $payment_summary .= "<tr><td>".t('Banking and transaction costs')."</td><td>$costs</td></tr>";
   if ($tax != '')
       $payment_summary .= "<tr><td class='tax'>".t('VAT !tax_value % on !tax_base', array('!tax_value'=>$tax_value, '!tax_base'=>(isset($costs) ? strtolower(t('Banking and transaction costs')) : 'total' ) ))."</td><td>$tax</td></tr>";
   $payment_summary .= "<tr><td class='total'>".t('TOTAL')."</td><td class='total'>$total_price $currency</td></tr></table>";
       
   
   $form['group']['payment_summary'] = array(
        '#value'=>$payment_summary
   );
   
   $shippable = module_invoke_all('hipay_field_shippable', $hipay_data['node']);
     
   if ( ($hipay_data['field']['shippable']==1 && !in_array(false, $shippable)) || ($hipay_data['field']['shippable']==0 && in_array(true, $shippable)) ) {
      $form['group']['shipping'] = array(
           '#type' => 'fieldset',
           '#attributes' => array('class'=>'shipping'),
           '#title' => t('Shipping information'),
           '#description' => t('Those information are private and will never be used for any communication neither transmitted to partners.').
                ($user->uid > 0 ? '<br>'.t('If you want to save those information for later use, please fill in your !account_link.', array('!account_link'=>l(t('private profile information'),'myaccount/editprivate',array('attributes' => array('target' => '_parent'))))) : ''), 
           '#weight'=>2,
           '#tree'=> true
      );   
      
      $form['group']['shipping']['shipping_lastname'] = array(
           '#type' => 'textfield',
           '#size' => 20,
           '#required'=> false,
           '#title' => t('Last name'),
           '#default_value' => !empty($user->field_lastname[0]['value']) ? $user->field_lastname[0]['value'] : '',
           '#weight'=>3,
      ); 
   
      $form['group']['shipping']['shipping_firstname'] = array(
           '#type' => 'textfield',
           '#size' => 20,
           '#required'=> false,
           '#title' => t('First name'),
           '#default_value' => !empty($user->field_firstname[0]['value']) ? $user->field_firstname[0]['value'] : '',
           '#weight'=>4,
      ); 
      
      $form['group']['shipping']['shipping_address'] = array(
           '#type' => 'textarea',
           '#cols' => 30,
           '#resizable' => FALSE,
           '#required'=> false,
           '#default_value' => !empty($user->field_address[0]['value']) && is_string($user->field_address[0]['value']) ? $user->field_address[0]['value'] : '',
           '#title' => t('Delivery address'),
           '#weight'=>5,
      );
   
      $form['group']['shipping']['shipping_zipcode'] = array(
           '#type' => 'textfield',
           '#size' => 20,
           '#required'=> false,
           '#title' => t('Zipcode'),
           '#default_value' => !empty($user->field_zipcode[0]['value']) ? $user->field_zipcode[0]['value'] : '',
           '#weight'=>6,
      ); 
      
      $form['group']['shipping']['shipping_city'] = array(
           '#type' => 'textfield',
           '#size' => 20,
           '#required'=> false,
           '#title' => t('City'),
           '#default_value' => '',
           '#default_value' => !empty($user->field_city[0]['value']) ? $user->field_city[0]['value'] : '',
           '#weight'=>7,
      ); 
   }
   $form['group']['message'] = array(
        '#type' => 'textarea',
        '#cols' => 20,
        '#default_value' => $form_state['values']['message'],
        '#title' => t('Personal message'),
        '#description' => '<p>'.t('You can add a personal message to the seller, some specifications, etc').'</p>' ,
        '#weight'=>8,
   );
   
   if (!empty($hipay_data['field']['cgu'])) {
        $form['group']['cgu'] = array(
          '#title' => t( 'I accept the terms of use: !tofuse', array('!tofuse'=>l(t('read'),'hipay_field/cgu/'.$hipay_data['field']['field_name'], array('attributes'=> array('rel'=>'lightframe[|width:660px; height:500px; scrolling: auto;]'))) )),
          '#type' => 'checkbox',
          '#weight' => 9,
        );
   }
    
   $form['group']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Pay your order'),
        '#weight'=>20,
        '#attributes' => array("onClick" => "javascript: this.value='".t('Contacting Hipay. Please Wait...')."'; this.disabled = true;$(this).after('<span class=\"views-throbbing\">&nbsp</span>');this.form.submit();"),
   );

   $form['nid'] = array(
        '#type' => 'hidden',
        '#value' => $hipay_data['node']->nid,
   );

   $form['field_name'] = array(
        '#type' => 'hidden',
        '#value' => $hipay_data['field']['field_name'],
   );

   //$form['#validate'][] = 'hipay_field_pay_form_validate';
   return $form;
}

function hipay_field_pay_form_validate($form, &$form_state) {
   $field = content_fields($form_state['values']['field_name']);
   //dsm($form);
   if (!empty($field['cgu']) && $form_state['values']['cgu']!= 1) {
       form_set_error('cgu', t('You must accept the terms of use to order.'));
       return false;
   } else 
       return true;
}

function hipay_field_pay_form_submit($form, &$form_state) {
   //print_r($form_state);
   $node = node_load($form_state['values']['nid']);
   
   $shipping = $form_state['values']['shipping'];
   //dsm($form_state);
   if ($node->nid > 0) {
     $result = hipay_field_make_transaction($node, $form_state['values']['field_name'], $form_state['values']['message'] , $shipping);  
   }

   if ($result['error'] === false) {
      $form_state['redirect'] = $result['redirect'];
   } else {
      drupal_set_message($result['error'], 'error');
   }
}

function hipay_field_manage_payment($responseCode='cancel', $orderReference=null) {
	if (_hipay_field_api_loadandwarn() === false)
		return;

	$redirect = "";
	$error_msg = "";
	$message = "";
	
	if ($orderReference != null) {
		$order = hipay_field_get_order_byref($orderReference);
		$node_order = $order->object;
		$user = user_load($node_order->uid);
	}
	
   	switch($responseCode) {
      case 'hipay_listen':
         // Réception de la notification depuis la plateforme Hipay
         // Le flux XML[C] est envoyé par POST, dans le champ " xml ".
         // La fonction analyzeNotificationXML traite le flux XML en provenance de la plateforme Hipay.
         $r = HIPAY_MAPI_COMM_XML::analyzeNotificationXML($_POST['xml'], &$operation, &$status, &$date, &$time, &$transid, &$amount,
         &$currency, &$idformerchant, &$merchantdatas, &$emailClient, &$subscriptionId, &$refProduct);
         // Une erreur s'est produite
         if ($r===false) {
            // Log de l'erreur dans un fichier texte sur le serveur
            watchdog('hipay_field', 'HIPAY ERROR IN RESPONSE',array(), WATCHDOG_ERROR);
         } else {
         // Le flux a été traité
         // Le marchand peut ici mettre à jour sa base de données de commandes et effectuer d'autres traitements.
            $answer = 
               "operation=$operation\n
               status=$status\n
               date=$date\n
               time=$time\n
               transaction_id=$transid\n
               amount=$amount\n
               currency=$currency\n
               idformerchant=$idformerchant\n
               merchantData=". print_r($merchantdatas,true)."\n
               emailClient=$emailClient\n
               subscriptionId=$subscriptionId\n
               refProduct=".print_r($refProduct,true);
         
            $hipay_data_db = array(    'date'=>(string)$date, 
                                    'time'=>(string)$time, 
                                    'transactionid'=>(string)$transid, 
                                    'emailclient'=>(string)$emailClient, 
                                    'subscriptionid'=>(string) $subscriptionId, 
                                    'refproduct'=>(string)serialize((array)$refProduct),
                                    'field_name'=>$merchantdatas['field_name'],
                                    'shipping'=>(string)$merchantdatas['shipping']);
            
            $status = strtolower($status);
            $operation = strtolower($operation);
            $shipping = unserialize($merchantdatas['shipping']);
            
            if ($status=='ok') {
               watchdog('hipay_field', 'Success for order: !answer .', array('!answer'=>$answer));
               switch ($operation) {
                  case 'authorization':
                     $status_hpf = HIPAY_FIELD_ORDER_STATUS_AUTHORIZED;
                     if (module_exists('privatemsg') && $merchantdatas['seller_id']>0) {
                        $node = node_load($merchantdatas['nid']);
                        $seller = privatemsg_user_load($merchantdatas['seller_id']);
                        $msg_author = privatemsg_user_load(1); // system admin
                        $buyer = $merchantdatas['customer_id'] > 0 ? privatemsg_user_load($merchantdatas['customer_id']) : $emailClient;
                        $buyer_link = is_object($buyer) ? l($buyer->name, 'user/'.$buyer->uid, array('absolute'=>true)) : $buyer;
                        $node_link = l($node->title, 'node/'.$node->nid, array('absolute'=>true));
                        $approve_link = l(t("approve"), 'hipay_field/payment/capture/confirm/'.$transid, array('absolute'=>true));
                        $cancel_link = l(t("cancel"), 'hipay_field/payment/capture/cancel/'.$transid, array('absolute'=>true));
                        $delay = format_date(strtotime("+"._hipay_confirmation_delay($order->capture)." day"), 'custom', 'd M H:i');
                        $message_body = "";
                        $shipping_infos = "";
                        if ($merchantdatas['message'] !='' && $merchantdatas['message'] != false) {
                           $message_body = "\n".t("Here is the customer's message:")."\n<cite>{$merchantdatas['message']}</cite>\n\n";
                        }
                        
                        if (!empty($shipping)) {
                           $shipping_infos  = "\n".t("Delivery address").":\n<cite>";
                           $shipping_infos .= _hipay_format_shipping($shipping);
                           $shipping_infos .= "</cite>";
                        }
                     
                        
                        $body_text = t("Hello !seller!\n
                        !buyer has just bought your speciality !product.\n
                        You must send your order to him before the !delay. Once you have done it, click on 'approve' to release and receive your payment.\n\n
                        If you don't accept your order, click on 'cancel'.\n\n
                        Click here to approve: !approve\n
                        Click here to refuse: !cancel\n\n
                        !message
                        !shipping
                        Enjoy!", array('!delay'=>$delay, '!seller'=>$seller->name, '!product'=>$node_link, '!buyer'=>$buyer_link, '!approve'=>$approve_link, '!cancel'=>$cancel_link, '!message'=>$message_body, '!shipping'=>$shipping_infos ));
                        
                        //watchdog('hipay_field', 'Message prepared: !to from: !from .', array('!to'=>print_r($seller,true),'!from'=>print_r($msg_author,true)));
                        privatemsg_new_thread(array($seller), t('New Reservation: !node_title', array('!node_title'=>$node->title)), $body_text, array('author'=>$msg_author) );
                     }
                     
                     break;
                 case 'capture': 
                     $status_hpf = HIPAY_FIELD_ORDER_STATUS_PAID;
                     break;
                  case 'cancellation':
                     $status_hpf = HIPAY_FIELD_ORDER_STATUS_CANCELLED;
                     break;
                  case 'refund':
                     $status_hpf = HIPAY_FIELD_ORDER_STATUS_REFUNDED;
                     break;
                  case 'rejet':
                  default:
                     $status_hpf = HIPAY_FIELD_ORDER_STATUS_REJECTED;
                     break;
               }
               
               _hipay_update_order_status((string)$idformerchant, 0, $status_hpf, $hipay_data_db);
               
            } elseif ($status=='nok') {
                watchdog('hipay_field', 'The order response: !answer .', array('!answer'=>$answer),WATCHDOG_ERROR);
                /*switch ($operation) {
                  
               }*/
            }
         }
         exit;
		break;
      case 'success':
        $vars = array('!product'=>$node_order->title, '!user'=>$user->name, '!product'=>$node_order->title, '!user'=>$user->name, '!delay'=>HIPAY_MAPI_CAPTURE_MAX_DAYS);
        $message = t('Thank you for purchasing !product to !user! !user was informed of your order.',$vars);
      	if ($order->capture == HIPAY_MAPI_CAPTURE_MANUAL) {
      		$message .= ' '.t("You have not been debited yet. The seller must accept your order within !delay days to receive your payment (confirmed to you by email). Otherwise you will automatically be refunded.", $vars);
      	}
      	$redirect = 'node/'.$node_order->nid;
      	break;
      case 'refused':
      	$error_msg .= t('Your order has been refused.');
      case 'cancel':
      	$error_msg .= t('Your order is cancelled.');
      default:
      	$redirect = 'node/'.$node_order->nid;
      	break;
   }
   
   if ($redirect != '') {
   		if ($message != '')
   			drupal_set_message($message);
   		if ($error_msg != '')
   			drupal_set_message($error_msg, 'error');
   			
   		drupal_goto($redirect);
   }
   //print_r($_POST);
}

function hipay_field_manage_capture($action, $transaction_id) {
   
   // check user owns product from reference or is admin or is affiliate
   $order = hipay_field_get_order_bytransaction($transaction_id);
   global $user;
   
   if (true || $order != false && (user_access('admin') || user_access('manage any capture') || $order->auid == $user->uid || $order->object->uid == $user->uid) ) {
   
      switch($action) {
         case 'confirm':
         case 'cancel':
            $soap = hipay_field_soap_object();
            $result = $soap->client->$action(array('parameters'=>array(
            'wsLogin' => $soap->login,
            'wsPassword' => $soap->password,
            'transactionPublicId' => $transaction_id
            )));
            // STEP 4 : Response
            $resultvar = $action.'Result';
            //print_r($result);
            $resultObj = $result->$resultvar;
            //sleep(4);
            if ( $resultObj->code == 0) {
               drupal_set_message(t('Transaction @ref is: @action. Update will appear shortly on screen.', array('@ref'=>$transaction_id, '@action'=>t($action))));
               drupal_goto();
               return true;
            } else {
               drupal_set_message(t("Error: @message. Your request is being processed by Hipay. Please wait...", array('@message'=>$resultObj->description)), 'error');
               drupal_goto();
               return false;
            }
            
            break;
         default:
            return false;
   }
   } else {
      return false;
   }
}


