<?php 

function hipay_field_soap_object() {
    $options = array(
    'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
    'cache_wsdl' => WSDL_CACHE_NONE,
    'soap_version' => SOAP_1_1,
    'encoding' => 'UTF-8'
    );
   
    $sandbox = variable_get(HIPAY_FIELD_SANDBOX_VAR, TRUE);
    $url = $sandbox==TRUE ? 'https://test-ws.hipay.com/soap/transaction-v2?wsdl' : 'https://ws.hipay.com/soap/transaction-v2?wsdl';
    $soabObj->client = new SoapClient($url, $options);
    $soabObj->login = variable_get(_hipay_field_get_variable(HIPAY_FIELD_LOGIN_VAR,$sandbox),'');
    $soabObj->password = variable_get(_hipay_field_get_variable(HIPAY_FIELD_PWD_VAR,$sandbox),'');
   
    return $soabObj;
}


function hipay_field_make_transaction($node, $field_name, $message='', $shipping=array(), $reference='' ) {
   global $user;
   $lang = isset($user->language) ? $user->language : 'fr';
   $result = array('redirect' =>'', 'error'=>false);
   
   if ( !_hipay_field_api_loadandwarn())
    return;
   
   $hipay_data = _hipay_field_get_payment_info($node,$field_name);
   $field = $hipay_data['field'];
   
   $affiliate_user = $hipay_data['affiliates'][0];
   $affiliate_infos = hipay_field_get_affiliate_account($hipay_data['affiliates'][0]);
   $affiliate_account = $affiliate_infos['account'];
   $affiliate_userid = $affiliate_infos['userid'];
   
   $gross_amount = $hipay_data['gross_price'];
   $net_amount = $hipay_data['net_price'];
   $tax_value = variable_get(HIPAY_FIELD_TAX_VALUE_VAR, 19.6);
   
   $sandbox = variable_get(HIPAY_FIELD_SANDBOX_VAR, TRUE);
   
   $reference = $reference=='' ? 'REF'.$user->uid.'-'.$node->nid.'-'.time().'-'.rand(1000,9999): $reference;
   $capture_day = $field['capture_day'];
   $currency = variable_get(_hipay_field_get_variable(HIPAY_FIELD_CURRENCY_VAR,$sandbox), 'EUR') ;
   $affiliate_tx = $field['affiliates'][0]['tx'];
   $site_tx = $field['site_costs'];
   
   $params = new HIPAY_MAPI_PaymentParams();
   // Paramètres de connexion à la plateforme Hipay. Attention, il ne s'agit pas du login et mot de passe utilisé pour se connecter
   // au site Hipay, mais du login et mot de passe propre à la connexion à la passerelle. Le login est l'id du compte associé au site,
   // le mot de passe est le « mot de passe marchand » associé au site.
   $params->setLogin( variable_get(_hipay_field_get_variable(HIPAY_FIELD_PRIMARY_ACCOUNT_VAR,$sandbox), '') , variable_get(_hipay_field_get_variable(HIPAY_FIELD_SITE_PWD_VAR,$sandbox), '') );
   // Les sommes seront créditées sur le compte 59118, sauf les taxes qui seront créditées sur le compte 59119
   $params->setAccounts( variable_get(_hipay_field_get_variable(HIPAY_FIELD_PRIMARY_ACCOUNT_VAR,$sandbox), ''), variable_get(_hipay_field_get_variable(HIPAY_FIELD_TAX_ACCOUNT_VAR,$sandbox), '') );
   // L'interface de paiement sera, par défaut, en français international
   $params->setLocale(hipay_field_hipay_lang($lang));
   // L'interface sera l'interface Web
   $params->setMedia('WEB');
   // Le contenu de la commande s'adresse à un public au moins âgé de 16 ans.
   $params->setRating($field['rating']);
   // Il s'agit d'un paiement simple
   $params->setPaymentMethod(HIPAY_MAPI_METHOD_SIMPLE);
   // La capture sera immédiate
   $params->setCaptureDay($capture_day);
   // Les montants sont donnés en euros, la devise associée au compte du site.
   $params->setCurrency( $currency );
   // L'identifiant au choix du commerçant pour cette commande est REF6522
   $params->setIdForMerchant($reference);
   // Deux données du type clé=valeur sont déclarées et seront retournées au commerçant après le paiement dans les
   // flux de notification.
   $params->setMerchantDatas('seller_id',$node->uid);
   $params->setMerchantDatas('message',$message);
   $params->setMerchantDatas('field_name',$field_name);
   $params->setMerchantDatas('customer_id',$user->uid);
   $params->setMerchantDatas('shipping',serialize($shipping));
   $params->setMerchantDatas('nid',$node->nid);
   // Cette commande se rapporte au site web qu'a déclaré le marchand dans la plateforme Hipay et qui a l'identifiant 9
   $params->setMerchantSiteId(variable_get(_hipay_field_get_variable(HIPAY_FIELD_SITE_ID_VAR,$sandbox), TRUE));
   // Si le paiement est accepté, le client est redirigé vers la page success.html
   $params->setUrlOk(url('hipay_field/payment/response/success/'.$reference,array('absolute'=>true)));
   // Si le paiement est refusé, le client est redirigé vers la page refused.html
   $params->setUrlNok(url('hipay_field/payment/response/refused/'.$reference,array('absolute'=>true)));
   // Si le paiement est annulé par le client, il est redirigé vers la page cancel.html
   $params->setUrlCancel(url('hipay_field/payment/response/cancel/'.$reference,array('absolute'=>true)));
   // L'email de notification du marchand posté en parallèle des notifications http sur l'url de ack
   // cf chap 19 : Réception de notification d'un paiement par le marchand
   //$params->setEmailAck('notification_hipay@monsite.com');
   // Le site du marchand est notifié automatiquement du résultat du paiement par un appel au script "listen_hipay_notification.php"
   // cf chap 19 : Réception de notification d'un paiement par le marchand
   $params->setUrlAck(url('hipay_field/payment/response/hipay_listen',array('absolute'=>true)));
   $t=$params->check();
   if (!$t) {
      echo "Erreur de création de l'objet paymentParams";
      exit;
   }
   
   $tax1 = new HIPAY_MAPI_Tax();
   $tax1->setTaxName('TVA ('.$tax_value.')');
   $tax1->setTaxVal($tax_value,true);
   $t=$tax1->check();
   if (!$t) {
      echo "Erreur de création de l'objet taxe";
      exit;
   }
   
   // Affilié qui recevra X% de tous les éléments de la commande
   if (!empty($affiliate_userid) && !empty($affiliate_account)) {
      $aff1 = new HIPAY_MAPI_Affiliate();
      $aff1->setCustomerId($affiliate_userid);
      $aff1->setAccountId($affiliate_account);
      //$aff1->setValue($affiliate_tx/(100+$site_tx)*100,HIPAY_MAPI_TTARGET_ALL);
      $aff1->setValue($affiliate_tx/100*$net_amount);
      $t=$aff1->check();
      if (!$t) { echo "Erreur de création de l'objet affiliate"; exit; }
   }
   
   // Premier produit : 2 exemplaires d'un livre à 12.50 euros l'unité sur (les taxes $tax3 et $tax2)
   $item1 = new HIPAY_MAPI_Product();
   $item1->setName($node->title.' - '.$affiliate_user->name);
   //$item1->setInfo($affiliate_user->name.' - product number '.$node->nid);
   $item1->setquantity(1);
   $item1->setRef('PROD'.$affiliate_user->uid.'-'.$node->nid);
   $item1->setCategory(626);
   // produit a un affilié et il est privé
   if (!empty($aff1) && $field['affiliates_type']==0) {
       $item1->setPrice($net_amount);
   } else {
      $item1->setPrice($net_amount/($tax_value+100)*100);
      $item1->setTax(array($tax1));
   }
   $t=$item1->check();
   if (!$t) { echo "Erreur de création de l'objet product"; exit; }
   
   if (!empty($aff1)) {
      $costs = $hipay_data['costs'];
      //$costs = $costs/($tax_value+100)*100;
      $item2 = new HIPAY_MAPI_Product();
      //$item2->setName("Transaction costs");
      $item2->setName(t('Banking and transaction costs', array('!com'=>round($costs/$net_amount*100,1)))); //setInfo
      $item2->setquantity(1);
      $item2->setRef('COMM');
      $item2->setCategory(200);
      $item2->setPrice($costs);
      $item2->setTax(array($tax1));
      $t=$item2->check();
      if (!$t) { echo "Erreur de création de l'objet product"; exit; }
   }
   
   $order = new HIPAY_MAPI_Order();
   // Titre et informations sur la commande
   $order->setOrderTitle(t("!seller's shop",array('!seller'=>$affiliate_user->name)));
   //$order->setOrderInfo('Les meilleurs produits !');
   
   // La catégorie de la commande est 3
   // cf annexe 7 pour savoir comment obtenir la liste des catégories disponibles pour votre site
   $order->setOrderCategory(626);
   // Les frais d'envoi sont de 1.50 euros HT, sur lesquels est appliquée la taxe $tax1
   //$order->setShipping(1.50,array($tax1));
   // Les frais d'assurance sont de 2 euros HT, sur lesquels sont appliquées les taxes $tax1 et $tax3
   //$order->setInsurance(2,array($tax3,$tax1));
   // Les coûts fixes sont de 2.25 euros HT, sur lesquels est appliquée la taxe $tax3
   //$order->setFixedCost($costs,array($tax1));
   // Cette commande a deux affiliés, $aff1 et $aff2
   if (!empty($aff1)) {
      $order->setAffiliate(array($aff1));
      $order_items = array($item2, $item1);
   } else {
      $order_items = array($item1);
   }
   $t=$order->check();
   if (!$t)
   {
   echo "Erreur de création de l'objet order";
   exit;
   }
   
   
   
   try {
   $commande = new HIPAY_MAPI_SimplePayment($params, $order, $order_items);
   }
   catch (Exception $e) {
   	echo " ORDER ERROR " .$e->getMessage();
   }
      
   $xmlTx = $commande->getXML();
   $output = HIPAY_MAPI_SEND_XML::sendXML($xmlTx);
   
   $r=HIPAY_MAPI_COMM_XML::analyzeResponseXML($output, &$url, &$err_msg);
   if ($r===true) {
   // On renvoie l'internaute vers l'url indiquée par la plateforme Hipay
   //header('Location: '.$url) ;
    $result['redirect'] = $url;
    
    $affiliate_amount = $commande->getAffiliateTotalAmount();
    $total_amount = $commande->getOrderTotalAmount();
    $oid = _hipay_insert_neworder($node, $user, $reference, $capture_day, $currency, $total_amount[0], $affiliate_user, $affiliate_amount[0] );
    $result['error'] = $oid === false ? 'ERROR DURING ORDER INSERT' : false;
   //echo "RESPONSE: ".$url;
   } else {
   // Une erreur est intervenue
   $result['error'] = "Erreur during payment creation: ".$err_msg;
   //echo "ERR: ".$err_msg;
   // $url_error = “/error.html”;
   //header('Location: '.$url_error) ;}
   }
    
   return $result;
}

function hipay_field_hipay_lang($lang='fr') {
   return $lang."_".($lang=='en'?'GB':strtoupper($lang));
}

// get affiliate account from drupal user
function hipay_field_get_affiliate_account($user) {
   static $affiliate_account = array();
   
   if (is_numeric($user)) {
        $user = user_load($user);
        $user = (object) array_merge((array)$user, (array)content_profile_load($user->type,$user->uid));
   }
   if (!isset($affiliate_account[$user->uid])) {
      $aff_account = '';
      foreach((array)$user as $key=>$item) {
         $field = content_fields($key); 
         if ($field!=NULL && !empty($item[0]) && $field['type'] == HIPAY_FIELD_NAME_AFFILIATES) {
            $aff_account = $item[0];
            break;
         }
      }
      $affiliate_account[$user->uid] = $aff_account;
   }
   
   return $affiliate_account[$user->uid];
}

// get affiliates from hipay account
function hipay_field_get_affiliate_user($account) {
   $types = content_types(HIPAY_FIELD_NAME_AFFILIATES);
   //dsm($types);
   
}

function hipay_field_get_order_bytransaction($transactionid) {
     $result = db_query("SELECT o.*, s.* FROM {hipay_field_orders} o LEFT JOIN {hipay_field_orders_status} s USING (oid) WHERE transactionid='%s' ORDER BY s.timestamp DESC LIMIT 1;", $transactionid);
     if ($data_order = db_fetch_object($result)) {
        if ( is_string($data_order->object))
            $data_order->object = unserialize($data_order->object);
        
        if ( is_string($data_order->hipay_data))
            $data_order->hipay_data = unserialize($data_order->hipay_data);
            
        return $data_order;
     }
     
     return false;
}

function hipay_field_get_order_byref($reference) {
	 $result = db_query("SELECT o.*, s.* FROM {hipay_field_orders} o LEFT JOIN {hipay_field_orders_status} s USING (oid) WHERE ref='%s' ORDER BY s.timestamp DESC LIMIT 1;", $reference);
	 if ($data_order = db_fetch_object($result)) {
	 	if ( is_string($data_order->object))
	 		$data_order->object = unserialize($data_order->object);
	 	
	 	if ( is_string($data_order->hipay_data))
	 		$data_order->hipay_data = unserialize($data_order->hipay_data);
	 		
	 	return $data_order;
	 }
	 
	 return false;
}

function hipay_field_get_user_pending_orders($uid, $history=false) {
     if ($uid=='all') {
        $uid=0;
        $show_all = true;
     } else {
        $show_all = false;
     }
     
     $history_timestamp = format_date(time() - variable_get('block_hipay_field_validation_archive_delay', 10)*60*60*24 , 'custom', 'Y-m-d H:i:s O');
     
     $result = db_query("SELECT o.*, f.uid as ouid, f.timestamp as purchase_timestamp, c.* FROM {hipay_field_orders} o 
                         LEFT JOIN (SELECT oid, max(timestamp) as latest, min(timestamp) as first FROM {hipay_field_orders_status} GROUP BY oid) d USING (oid)
                         LEFT JOIN hipay_field_orders_status c ON o.oid=c.oid AND c.timestamp=d.latest
                         LEFT JOIN hipay_field_orders_status f ON o.oid=f.oid AND f.timestamp=d.first
                         WHERE auid".($show_all?">":"")."=%d AND (c.status=%d ".($history ? "OR (c.status>%d AND c.timestamp>%d)" : "").") GROUP BY o.oid ORDER BY c.timestamp DESC;", $uid, HIPAY_FIELD_ORDER_STATUS_AUTHORIZED,HIPAY_FIELD_ORDER_STATUS_AUTHORIZED,$history_timestamp);
     $pending_orders = array();
     while ($data_order = db_fetch_object($result)) {
        if ( is_string($data_order->object))
            $data_order->object = unserialize($data_order->object);
        
        if ( is_string($data_order->hipay_data))
            $data_order->hipay_data = unserialize($data_order->hipay_data);
        
        $pending_orders[] = $data_order;
     }
     
     return $pending_orders;
}

function _hipay_field_get_variable($var, $use_sandbox=false) {
   return isset($use_sandbox) && $use_sandbox != false ? $var.'_sandbox' : $var;
}

function _hipay_field_get_payment_info($node, $field_name) {

   static $payments_info = array();
   
   if (is_numeric($node))
        $node = node_load($node);
   
   if (!isset($payments_info[$node->nid.'-'.$field_name])){
      $hipay_data = false;
      $field = content_fields($field_name);
      list($price_node_type, $price_node_field) = explode('---', $field['price_field']);
      // check if the field is on a compatible node
      $price_field = $node->$price_node_field;
      if ($node->type != $price_node_type || !isset($price_field) || ( !is_array($price_field[0]) && empty($price_field[0]['value']) ))
        return false;
      
      // set order net price 
      $net_price = $price_field[0]['value'] * 1;
      
      $site_tx = $field['site_costs'];
      
      $affiliate_uid = $node->uid;
      $affiliate_setup = $field['affiliates'][0];
      list($node_type, $node_field) = explode('---', $affiliate_setup['account_field']);
      $affiliate_hipay_node = node_load(array('uid'=>$affiliate_uid, 'type'=>$node_type));
      $affiliate_user = user_load($affiliate_uid);
      $affiliate_user = (object) array_merge((array)$affiliate_user, (array)content_profile_load($affiliate_user->type,$affiliate_uid));
      $tax_value = variable_get(HIPAY_FIELD_TAX_VALUE_VAR, 19.6);
      
      $gross_price = $net_price *(100 + $site_tx) / 100;
      // pour les affiliés pros, il faut facturer la TVA
      /*if ( $field['affiliates_type']==1)
        $gross_price = $gross_price + $net_price*variable_get(HIPAY_FIELD_TAX_VALUE_VAR, 19.6)/100;*/
      
      $sandbox = variable_get(HIPAY_FIELD_SANDBOX_VAR, TRUE);

      $gross_price += HIPAY_FIELD_FIXED_FEES;
      $costs = ($gross_price - $net_price)/(1+$tax_value/100);
      
      if (!empty($affiliate_hipay_node->$node_field) && user_access('can be affiliate' ,$affiliate_user))
           $hipay_data = array('gross_price'=>$gross_price, 'net_price'=>$net_price,'costs'=>$costs, 'affiliates'=>array($affiliate_user), 'node'=>$node, 'field'=>$field);
           
      $payments_info[$node->id.'-'.$field_name] = $hipay_data;
   }
   
   return $payments_info[$node->id.'-'.$field_name];
   
}

function _hipay_confirmation_delay($field) {
   if ( !_hipay_field_api_loadandwarn()) { return; }
   
  return $field['capture_day']==HIPAY_MAPI_CAPTURE_MANUAL ? HIPAY_MAPI_CAPTURE_MAX_DAYS : $field['capture_day'] ;
}

function _hipay_insert_neworder($node, $buyer, $ref, $capture, $currency, $amount, $affiliate=null, $affiliate_amount=0) {
   $data = array(
    'ref'            => $ref,
    'auid'           => $affiliate==null? 0 : $affiliate->uid,
    'owner_amount'   => $amount,
    'aff_amount'     => $affiliate_amount,
    'cur'            => $currency,
    'capture'        => $capture,
    'object'         => $node
  );
  $new_order = drupal_write_record('hipay_field_orders', $data);
  global $user;
  if ($new_order!=FALSE) {
     $data_status = array(
       'oid'     => $data['oid'],
       'uid'     => $user->uid,
       'status'  => HIPAY_FIELD_ORDER_STATUS_INITED,
       'timestamp'    => format_date(time(), 'custom', 'Y-m-d H:i:s O'),
       'hipay_data'     => null,
     );
     $new_order_status = drupal_write_record('hipay_field_orders_status', $data_status);
     
     if ($new_order_status!=FALSE) {
          return $data['oid'];
     } else {
        db_query("DELETE FROM {hipay_field_orders} WHERE iod=%d LIMIT 1;", $data['oid']);
     }
  }
  watchdog('hipay_field', 'The order referenced !ref purchased by %user (%node) failed.', array('!ref'=>$ref, '%user'=>$user->uid, '%node'=>$node->nid), WATCHDOG_ERROR);
  return false;
}

function _hipay_update_order_status($reference, $uid, $status, $hipay_data=null) {
   
   $result = db_query("SELECT oid, transactionid FROM {hipay_field_orders} WHERE ref='%s' LIMIT 1;", $reference);
   $data_order = db_fetch_array($result);
   if ($result !==false && !empty($data_order)) {
      $data = array(
       'oid'            => $data_order['oid'],
       'uid'            => $uid,
       'status'         => $status,
       'timestamp'      => format_date(time(), 'custom', 'Y-m-d H:i:s O'),
       'hipay_data'     => $hipay_data,
     );
     $order_status = drupal_write_record('hipay_field_orders_status', $data);
     
     if ($data_order['transactionid'] == '') {
        db_query("UPDATE {hipay_field_orders} SET transactionid='%s' WHERE oid=%d LIMIT 1;", $hipay_data['transactionid'], $data_order['oid']);
     }
     //watchdog('hipay_field', 'Update order status for ref: !ref (!oid) to status !status by user !user', array('!ref'=>$reference, '!user'=>$uid, '!status'=>$status, '!oid'=>$data_order['oid']));
     
     return $order_status;
   } else {
      watchdog('hipay_field', 'Update order status failed for: !ref to status !status by user !user: !mysql_error', array('!ref'=>$reference, '!user'=>$uid, '!status'=>$status, '!mysql_error'=>mysql_error()), WATCHDOG_ERROR);
      return false;
   }
}

function _hipay_field_i8n_filename($filename) {
   global $language;
   $infos = pathinfo($filename);
   switch ($language->language) {
      case 'fr':
         $filename = $infos['filename'].'_'.$language->language.'.'.$infos['extension'];
         break;
   }
   
   return $filename;
}

function _hipay_format_shipping($shipping) {
   if (empty($shipping))
        return '';
        
   $shipping_infos = t("Name").": {$shipping['shipping_lastname']} {$shipping['shipping_firstname']}<br>\n";
   $shipping_infos .= t("Address").": {$shipping['shipping_address']}<br>\n";
   $shipping_infos .= t("Zip code").": {$shipping['shipping_zipcode']}<br>\n";
   $shipping_infos .= t("City").": {$shipping['shipping_city']}<br>\n";
   return $shipping_infos;
}

function _hipay_field_i8n_addcss() {
   global $language;
   switch($language->language) {
      case 'fr':
         $suffix = '_'.$language->language;
         break;
   }
   drupal_add_css(drupal_get_path('module', 'hipay_field').'/hipay_field'.$suffix.'.css');
}

?>