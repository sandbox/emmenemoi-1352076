<?php

define('HIPAY_FIELD_ACCOUNT_MAX_LENGTH',10);

/**
 * Implementation of cck hook_field_settings() for affiliates field
 */
function _hipay_field_affiliates_field_settings($op, $field) {

   //$value = text_field_settings($op, $field);
    switch ($op) {
       case 'database columns':
            $columns['userid'] = array('type' => 'varchar', 'length' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH, 'not null' => FALSE, 'sortable' => TRUE, 'views' => TRUE, 'default'=>'');
            $columns['account'] = array('type' => 'varchar', 'length' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH, 'not null' => FALSE, 'sortable' => TRUE, 'views' => TRUE, 'default'=>'');
            $columns['verified'] = array('type' => 'int', 'size' => 'tiny', 'not null' => TRUE,'views' => TRUE, 'default'=>0);
            return $columns;
            break;
       case 'form':
            $form = array();
            $text_cgu = module_exists('i18nstrings') ? i18nstrings('cck:field:'. $field['field_name'] .':widget_cgu',$field['cgu']) : $field['cgu'];
            $form['cgu'] = array(
                 '#type' => 'textarea',
                 '#cols' => 20,
                 '#default_value' => $text_cgu,
                 '#title' => t('Terms of use of the field'),
                 '#description' => '<p>'.t('You can add terms of use to be accepted. Leave blank if no terms of use.').'</p>' ,
                 '#weight'=>8,
            );
            $form['multiple'] = array(
              '#type' => 'hidden',
              '#value' => '0',
            );
            return $form;
            break;
       case 'validate':
          if( module_exists('i18nstrings'))
            i18nstrings_update('cck:field:'. $field['field_name'] .':widget_cgu',$field['cgu']);
          break;
       case 'save':
          return array('cgu');
     }
   
     //dsm($value);
     //return $value;    
}

/**
 * Implementation of hook_field().
 */
function _hipay_field_affiliates_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'sanitize':
      foreach ($items as $delta => $item) {
        foreach ( $item as $col => $dat ) {
          $items[$delta]['safe_' . $col ] = check_plain($item[ $col ]);
        }
      }
      break;
  }
}

/**
 * Theme function for 'hipay_affiliates' text field formatter.
 */
function theme_hipay_field_formatter_hipay_affiliates($element) {
  if(empty($element['#item'])  ) {
    return '';
  }
  return $element['#item']['account'];
}

function _hipay_field_affiliates_widget_settings($op, $widget) {
   //return text_widget_settings($op, $widget);
}

function _hipay_field_affiliates_widget(&$form, &$form_state, $field, $items, $delta = 0) {
   $element = array(
    '#type' => $field['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
  );
  
  return $element;
}

function _hipay_field_hipay_affiliates_process($element, $edit, &$form_state, &$form) {
   //dsm($form_state);
   //global $user;
   //dsm($user);
   //dsm($element);
   // process: capture account -> send validation -> store 
  //ahah_helper_register($form, $form_state);
  _hipay_field_i8n_addcss();
  $values = $element['#value'];
  $defaults = $element['#default_value'];
  $field = content_fields($element['#field_name'], $element['#type_name']);
  $field_key = $element['#columns'][0];

  //$element['#prefix'] = '<div id="validate-wrapper">';
 // $element['#suffix'] = '</div>';
  //$element['#tree'] = true;
  
  $url = variable_get(HIPAY_FIELD_SANDBOX_VAR, FALSE)==true ? HIPAY_REGISTRATION_URL_SANDBOX : HIPAY_REGISTRATION_URL;
  $url_parts = parse_url($url);
  $reset = ( $values['account']=='' && $values['userid']=='');
  $account_value = $values['account']!='' || $reset ? $values['account'] : $defaults['account'];
  $userid_value = $values['userid']!='' || $reset ? $values['userid'] : $defaults['userid'];
  
  $base_path = url(drupal_get_path('module','hipay_field'));
  $base_image = $base_path.'/misc/transparent.png';
  $register_desc = $defaults['account']=='' ? 
                            "<legend style='display:inline'>".t('SIGN IN OR CREATE AN HIPAY ACCOUNT')."</legend><br>
                            ".t('Click on the button and follow the instructions on the Hipay website.')."<br>".
                            l('<img src="'.$base_image.'" class="hipay_field_button hipay_field_sprites">','hipay_field/hipay_register', array('attributes'=>array('rel'=>'lightframe[|width:1024px; height:700px; scrolling: auto;][HiPay account registration]'), 'html'=>true))."<br>" : 
                            "<legend style='display:inline'>".t('VIEW MY HIPAY ACCOUNT')."</legend><br>".
                            l(t('view your Hipay account'),$url_parts['scheme']."://".$url_parts['host']."/auth", array('attributes'=>array('rel'=>'lightframe[|width:1024px; height:700px; scrolling: auto;][HiPay account]')))."<br>";

  $account_desc = $defaults['account']=='' ? 
                            "<legend style='display:inline'>".t('INDICATE YOUR HIPAY ACCOUNT INFORMATION FOR YOUR SHOP')."</legend><br>".
                            t( 'You can indicate your Hipay account here to display a "payment button" on each products and be able to receive directly payments anonymously.<br> !where', array('!where'=>l(t('Where can i find necessary information on Hipay website?'), 'sites/all/modules/hipay_field/misc/'._hipay_field_i8n_filename('hipay_screenshot.png'),array('attributes'=>array('rel'=>'lightbox'))))) :
                            "<legend style='display:inline'>".t( 'Account confirmed and ready to receive payments.')."</legend>";
  
                            
  $element['register'] = array(
    '#value'  => '<img class="hipay_field_one hipay_field_sprites" src="'.$base_image.'">'.$register_desc,
    '#weight' => 0,
    '#prefix' => '<div style="">',
    '#suffix' => '</div>',
  );

  $element['userid'] = array(
    '#title' => t( 'Hipay User ID' ),
    '#type' => 'textfield',
    '#size' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH,
    '#maxlength' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH,
    ($field_type == 'textfield' ? '#default_':'#').'value' => $userid_value,
    '#weight' => 1,
    //'#description' => t('Your Hipay user id'),
    '#attributes'=>array('class'=>'no-copypaste'),
    '#prefix' => '<img class="hipay_field_two hipay_field_sprites" src="'.$base_image.'">'.$account_desc.'<div class="hipay_field_account_fields">'
  );
 
  //$field_post = !empty($element['#post'][$element['#field_name']]) ? $element['#post'][$element['#field_name']][0] : NULL;
  //$field_type = ( ($field_post != NULL && $field_post['account']!='') || ($defaults['account']!='' && $defaults['verified']>0) ) ? 'hidden' : 'textfield';
  //$field_type = ($defaults['account']!='' && $field_post['account']=='') ? 'item' : $field_type;  
  /*if (preg_match('#send_validate#i',$form_state['clicked_button']['#id']) && $field_post['account']!='')
    hipay_field_make_transaction($account_value,0,t('VALIDATION REF: @ref',array('@ref'=>rand(1,99))));    */
  
  $element['account'] = array(
    '#title' => t( 'Hipay Account' ),
    '#type' => 'textfield',
    '#size' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH,
    '#maxlength' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH,
    ($field_type == 'textfield' ? '#default_':'#').'value' => $account_value,
    '#weight' => 2,
    //'#description' => '',
    '#attributes'=>array('class'=>'no-copypaste'),
    '#suffix'=>'</div>'
  );
  
  if (!empty($field['cgu']) && empty($defaults['account'])  && empty($defaults['userid']) ) {
     $element['cgu'] = array(
       '#title' => t( 'I accept the terms of use: !tofuse', array('!tofuse'=>l(t('read'),'hipay_field/cgu/'.$element['#field_name'], array('attributes'=> array('rel'=>'lightframe[|width:660px; height:500px; scrolling: auto;]'))) )),
       '#type' => 'checkbox',
       '#weight' => 3,
     );
  }
  
  /*
  if ($defaults['account']=='' || ($values['account']!=$defaults['account'] && (empty($values['account2']) || $values['account']!=$values['account2']) ) ) {
    $element[$element['#field_name']]['account2'] = array(
       '#title' => t( 'Hipay Account Confirmation' ),
       '#type' => 'textfield',
       '#size' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH,
       '#maxlength' => HIPAY_FIELD_ACCOUNT_MAX_LENGTH,
       '#default_value' => $values['account2'],
       '#weight' => 3,
       '#description' => t('Please confirm your account number to avoid mistakes.'),
     );
  }*/
  
 /* if ($confirm) {
     $element['send_validate'] = array(
       '#value' => t( 'Confirm Account' ),
       '#type' => 'button',
       '#size' => 2,
       '#maxlength' => 2,
       '#weight' => 4,
       //'#submit' => array('ahah_helper_submit'),
       );     
  } else if($defaults['verified']!=100) {
     $element['send_validate'] = array(
       '#value' => t( 'Validate Account' ),
       '#type' => 'button',
       '#size' => 2,
       '#maxlength' => 2,
       '#weight' => 4,
       '#ahah' => array(
         'event' => 'click', // When the button is "clicked", AHAH will do it's job
         'path' => ahah_helper_path(array_merge($element['#array_parents'])), // The array features the wrapper form field. So our form wrapper is $form['cars'], so we set this to array('cars'). If your form was $form['cars']['another_wrapper'], the path would be array('cars', 'another_wrapper').
         'wrapper' => 'validate-wrapper', // We then define the wrapper which will be changed.
           ),
       //'#submit' => array('ahah_helper_submit'),
       );
  }
  
   $element['verified'] = array(
      '#value' => $defaults['account']==$defaults['account2'] ? 100 : $defaults['verified'],
      '#type' => 'hidden',
      '#weight' => 10,
      //'#submit' => array('ahah_helper_submit'),
   );   */
    /*
    // 0: nothing sent and 100 = validated
    if ($defaults['verified']>0 && $defaults['verified']!= 100) {
     $element['verified'] = array(
       '#title' => t( 'Verification code' ),
       '#type' => 'textfield',
       '#size' => 2,
       '#maxlength' => 2,
       '#default_value' => $defaults['verified'],
       '#weight' => 4,
       '#description' => t('Indicate the code recieved on your Hipay account'),
     );
    }
    */
  // Used so that hook_field('validate') knows where to flag an error.
  $element['_error_element'] = array(
    '#type' => 'value',
    '#value' => implode('][', array_merge( $element['#parents'], array($field_key))),
  );
  return $element;
}

function _hipay_field_hipay_affiliate_validate(&$element, &$form_state) {
  //dsm($element);
  
    // clean error element
  $error_element = isset($element['_error_element']) ? $element['_error_element']['#value'] : '';
  if (is_array($element) && isset($element['_error_element'])) unset($element['_error_element']);
  
  if (empty($element['account']['#value']) && empty($element['userid']['#value'])) {
    return true;
  }
  if (empty($element['#value']['account']) || empty($element['#value']['userid'])) {
    form_set_value($element, '', $form_state);
  }

  $field = content_fields($element['#field_name'], $element['#type_name']);
  $ftitle = $field['widget']['label'];
  //dsm($field);
  //dsm($element);
  if (!empty($field['cgu']) && !empty($element['cgu']) &&  $element['cgu']['#value'] != 1)
    form_set_error($error_element, t('You must accept the terms of use to use the payments interface.'));

  if ( !preg_match('/^[0-9]+$/',$element['userid']['#value'])) {
    form_set_error($error_element, t('The user account specified for %field is not valid.', array('%field' => $ftitle )));
  }

  if ( !preg_match('/^[0-9]+$/',$element['account']['#value'])) {
    form_set_error($error_element, t('The account specified for %field is not valid.', array('%field' => $ftitle )));
  }
  /*if ( $element['account']['#value'] != $element['account2']['#value'] && $element['#value']['account']!='') {
    form_set_error($error_element, t('The account specified for %field has not been correctly confirmed.', array('%field' => $ftitle )));
  }*/
}


function theme_hipay_affiliate_widget($element) {
  drupal_add_js('sites/all/modules/hipay_field/hipay_field.hipay.js', 'module', 'header',FALSE, FALSE);// to cache, set last to TRUE
  return '<fieldset' . drupal_attributes($element['#attributes']) . ' class="hipay_field">' . ($element['#title'] ? '<legend>' . $element['#title'] . '</legend>' : '') . (isset($element['#description']) && $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '') . (!empty($element['#children']) ? $element['#children'] : ''). "</fieldset>\n";
}

function _hipay_field_affiliates_content_is_empty($item, $field) {
  if (empty($item['account']) && empty($item['userid']) ) {
      return TRUE;
  }
  return FALSE;
}

function _hipay_field_get_hipay($page, $proxy_path=null) {
   global $user;
   $lang = isset($user->language) ? $user->language : 'fr';

   $url = variable_get(HIPAY_FIELD_SANDBOX_VAR, FALSE)==true ? HIPAY_REGISTRATION_URL_SANDBOX : HIPAY_REGISTRATION_URL;
   $url_parts = parse_url($url);

   switch($page){
      case 'proxy':
         echo call_user_func('_hipay_field_get_'.$page, $lang,$url_parts['scheme']."://".$url_parts['host'],$proxy_path);
         break;
      case 'register':
      default:
         echo (call_user_func('_hipay_field_get_'.$page,$lang,$url_parts['scheme']."://".$url_parts['host'],$url_parts['path']));
         break;
   }
   //exit;
}

function _hipay_field_get_register($lang='fr',$base='',$proxy_path='/') {
   global $user;
   $url = $base.$proxy_path;
   if (extension_loaded('apc')) {
      $cached_var = unserialize(apc_fetch(HIPAY_FIELD_APC));
      if ($cached_var != null)
        $file = $cached_var[md5($url)];
   }
   
   if ($file==null) {
      $opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"Accept-language: ".$lang."\r\n" .
                    "Cookie: foo=bar\r\n"
           )
      );
      
      $context = stream_context_create($opts);
      
      // Open the file using the HTTP headers set above
      $file = file_get_contents($url, false, $context);
      if (extension_loaded('apc')) {
        $cached_var[md5($url)] = $file;
        apc_store(HIPAY_FIELD_APC,serialize($cached_var), 60*60*24);
      }
   }
   $fileCorrected = $file;
   // keep original domain
   //$regex = "/(src\s*=\s*['\"]|href\s*=\s*['\"])([^'\"]+)(['\"][^>]*>)/i";
   //$fileCorrected = preg_replace($regex, "$1https://www.hipay.com$2$3", $file);
   $fileCorrected = preg_replace("#(<head>)(.*?)(</head>)#is", "$1<base href=\"".$base."\" />$2$3", $file);
   //$fileCorrected = preg_replace("#(<head>)(.*?)(</head>)#is", "$1<base href=\"".url("hipay_field/hipay_register",array('absolute'=>true))."\" />$2$3", $file);   
   
   // tune page
   $fileCorrected = preg_replace("#<ul class=\"menu\">.*?</ul>#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#<td>\s*<input id=\"radio_button_pro\".*?</td>#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#<tr>\s*<th\s*>Compte.*?Compra Facil.*?</tr>#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#<!-- Compte de test -->.*?<!-- fin Compte de test -->#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#(<label><input[\w=\s'\"]*hipay_info['\"\s]*?value=\"1\")\s*?(/>[\w\s]*?</label>)#is", "$1 checked $2", $fileCorrected);
   $fileCorrected = preg_replace("#(<label><input[\w=\s'\"]*hipay_part['\"\s]*?value=\"1\")\s*?(/>[\w\s]*?</label>)#is", "$1 checked $2", $fileCorrected);
      
   
   // add necessary informations.
   $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"email1\"\s*id=\"email1\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->mail."$2", $fileCorrected);
   $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"email2\"\s*id=\"email2\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->mail."$2", $fileCorrected);
   $fileCorrected = preg_replace("#(<option\s*value=\"EUR\"\s*label=\"Euro\"\s*)(>\s*Euro\s*</option>)#is", "$1 selected $2", $fileCorrected);

   //dsm($user);
   
   if (!empty($user->field_sex)) {
       $sex = $user->field_sex[0]['value'] == 'M' ? '1' : '2';
       $fileCorrected = preg_replace("#(<select\s*name=\"civilite\".*?<option\s*value=\"".$sex."\" label=[\"\w]*)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
   }
   if (!empty($user->field_firstname)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"prenom\"\s*id=\"prenom\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->field_firstname[0]['value']."$2", $fileCorrected);
             
   if (!empty($user->field_lastname)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"nom\"\s*id=\"nom\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->field_lastname[0]['value']."$2", $fileCorrected);

   if (!empty($user->field_address)) 
        $fileCorrected = preg_replace("#(<textarea\s*name=\"adresse[\s\"\w\d=']*>)\s*(</textarea>)#is", "$1 ".$user->field_address[0]['value']." $2", $fileCorrected);

   if (!empty($user->field_zipcode)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"code_postal\"\s*id=\"code_postal\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1 ".$user->field_zipcode[0]['value']." $2", $fileCorrected);

   if (!empty($user->field_city)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"ville\"\s*id=\"ville\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->field_city[0]['value']."$2", $fileCorrected);
   
   if (!empty($user->field_country)) {
      $country_regex = "#(<select[\s\w\"'=]*?id=\"pays\">.*?<option[\s\w\"'=]*?label=\"".strtoupper($user->field_country[0]['value'])."\"\s*)(>.*?</select>)#is";
      $fileCorrected = preg_match($country_regex, $fileCorrected) ? preg_replace($country_regex, "$1 selected $2", $fileCorrected) : $fileCorrected;
   }

   $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"language\">.*?<option[\s\w\"'=]*?value=\"".hipay_field_hipay_lang($lang)."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
   $fileCorrected = preg_replace("#(<option\s*value=\"Europe/Paris\"[\s\w\"'=]*?)(.*?</option>)#is", "$1 selected $2", $fileCorrected);
   
   if (!empty($user->field_birthday)) {
        $birthday = strtotime($user->field_birthday[0]['value']);
        $birthday_day = date('j',$birthday);
        $birthday_month = date('n',$birthday);
        $birthday_year = date('Y',$birthday);  

        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"Date_Day\">.*?<option[\s\w\"'=]*?value=\"".$birthday_day."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"Date_Month\">.*?<option[\s\w\"'=]*?value=\"".$birthday_month."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"Date_Year\">.*?<option[\s\w\"'=]*?value=\"".$birthday_year."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
                        
   }
   //$fileCorrected .= "<script>jQuery(document).ready(function($) { initHipayCatcher(); });</script>";
   //$fileCorrected .= '<br><br>'.$url;
   
   return $fileCorrected; 
}



