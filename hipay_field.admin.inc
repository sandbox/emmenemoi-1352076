<?php 

/**
 * Callback for admin/settings/hipay_field
 * Defines the settings form using FAPI.
 */
function hipay_field_settings(&$form_state, $sandbox=null) {
  $form = array();
  _hipay_field_api_loadandwarn();
  
  $form['message1']['#value']="<div class='message".(variable_get(HIPAY_FIELD_SANDBOX_VAR, TRUE)?' warning':' status' )."' style='font-weight:bold;padding:10px 0px 10px 10px;'>".($sandbox!=null?'SANDBOX':'MAIN')." parameters (sandbox ".(variable_get(HIPAY_FIELD_SANDBOX_VAR, FALSE) ? 'ACTIVATED':'NOT ACTIVATED').")</div>";
  $form['message2']['#value']="<div class='message' style='padding:10px 0 10px 10px;margin-top:5px;background:#eee;border:1px solid black;'>CURRENT ACK URL: ".HIPAY_GATEWAY_URL."</div>";

  $form[HIPAY_FIELD_SANDBOX_VAR] = array(
    '#description' => t('Should we use the SANDBOX environment.'),
    '#default_value' => variable_get(HIPAY_FIELD_SANDBOX_VAR, TRUE),
    '#title' => t('Activate Sandbox'),
    '#type' => 'checkbox',
  );
  
  $form[HIPAY_FIELD_TAX_VALUE_VAR] = array(
    '#description' => t('Indicate the tax value that should be applied, in percent'),
    '#default_value' => variable_get(HIPAY_FIELD_TAX_VALUE_VAR, 19.6),
    '#title' => t('Tax value'),
    '#type' => 'textfield',
    '#size' => 4,
    '#max_length' => 4,
  );
  
  $form[_hipay_field_get_variable(HIPAY_FIELD_SITE_ID_VAR,$sandbox)] = array(
    '#description' => t('ID of the website registered with Hipay.'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_SITE_ID_VAR,$sandbox), TRUE),
    '#title' => t('Website ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#size'=> 5,
  );
  
  $form[_hipay_field_get_variable(HIPAY_FIELD_SITE_PWD_VAR,$sandbox)] = array(
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_SITE_PWD_VAR,$sandbox), ''),
    '#required' => TRUE,
    '#title' => t('Merchant web site password)'),
    '#type' => 'textfield',
  );
  
  $form[_hipay_field_get_variable(HIPAY_FIELD_LOGIN_VAR,$sandbox)] = array(
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_LOGIN_VAR,$sandbox), ''),
    '#required' => TRUE,
    '#title' => t('Merchant webservice login'),
    '#type' => 'textfield',
  );

   $form[_hipay_field_get_variable(HIPAY_FIELD_PWD_VAR,$sandbox)] = array(
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_PWD_VAR,$sandbox), ''),
    '#required' => TRUE,
    '#title' => t('Merchant webservice password'),
    '#type' => 'textfield',
  );
  $currencies = array();
   foreach (explode(',', HIPAY_FIELD_CURRENCIES) as $value) {
       $currencies[trim($value)] = trim($value);
   }
  
   $form[_hipay_field_get_variable(HIPAY_FIELD_CURRENCY_VAR,$sandbox)] = array(
    '#description' => t('Main currency that will be used for each account'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_CURRENCY_VAR,$sandbox), 'EUR'),
    '#options' => $currencies,
    '#required' => TRUE,
    '#title' => t('Main currency'),
    '#type' => 'select',
  );
  
   $form['hipay_field_accounts'] = array(
    '#description' => t('Which accounts will you use?'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Merchant accounts'),
    '#type' => 'fieldset',
  );
  
  $form['hipay_field_accounts'][_hipay_field_get_variable(HIPAY_FIELD_PRIMARY_ACCOUNT_VAR,$sandbox)] = array(
    '#description' => t('Which is the primary account that will be used for payments?'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_PRIMARY_ACCOUNT_VAR,$sandbox), ''),
    '#required' => TRUE,
    '#title' => t('Primary account'),
    '#type' => 'textfield',
  );

  $form['hipay_field_accounts'][_hipay_field_get_variable(HIPAY_FIELD_TAX_ACCOUNT_VAR,$sandbox)] = array(
    '#description' => t('Which is the secoundary account that will be used as tax account? If none provided, main account will be used.'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_TAX_ACCOUNT_VAR,$sandbox), ''),
    '#required' => FALSE,
    '#title' => t('Tax account'),
    '#type' => 'textfield',
  );

  $form['hipay_field_accounts'][_hipay_field_get_variable(HIPAY_FIELD_FIXED_COSTS_ACCOUNT_VAR,$sandbox)] = array(
    '#description' => t('Which is the secoundary account that will be used as fixed costs account? If none provided, main account will be used.'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_FIXED_COSTS_ACCOUNT_VAR,$sandbox), ''),
    '#required' => FALSE,
    '#title' => t('Fixed costs account'),
    '#type' => 'textfield',
  );
  
  $form['hipay_field_accounts'][_hipay_field_get_variable(HIPAY_FIELD_INSURANCE_ACCOUNT_VAR,$sandbox)] = array(
    '#description' => t('Which is the secoundary account that will be used as insurance cost accounts? If none provided, main account will be used.'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_INSURANCE_ACCOUNT_VAR,$sandbox), ''),
    '#required' => FALSE,
    '#title' => t('Insurances account'),
    '#type' => 'textfield',
  );
  
  $form['hipay_field_accounts'][_hipay_field_get_variable(HIPAY_FIELD_SHIPPING_ACCOUNT_VAR,$sandbox)] = array(
    '#description' => t('Which is the secoundary account that will be used as shipping costs accounts? If none provided, main account will be used.'),
    '#default_value' => variable_get(_hipay_field_get_variable(HIPAY_FIELD_SHIPPING_ACCOUNT_VAR,$sandbox), ''),
    '#required' => FALSE,
    '#title' => t('Insurances account'),
    '#type' => 'textfield',
  );
  
  $form['#submit'][] = 'hipay_field_settings_submit';
  return system_settings_form($form);
}

function hipay_field_settings_submit($form, &$form_state) {
    //dsm($form_state);
    if ($form_state['values']['hipay_field_sandbox']!=variable_get(HIPAY_FIELD_SANDBOX_VAR, FALSE)){
       $filename= libraries_get_path('hipay_mapi_php5') . '/mapi_defs.php';
       $config = file_get_contents($filename);
       $subdomain = $form_state['values']['hipay_field_sandbox']==1 ? 'test-' : '';
       $config = preg_replace("#(https://)(test-|)(payment.hipay.com)#is","$1".$subdomain."$3",$config);
       file_put_contents($filename, $config);
       //dsm($form_state);
   }
   
   if (extension_loaded('apc')) {
      apc_delete(HIPAY_FIELD_APC);
   }
}

?>