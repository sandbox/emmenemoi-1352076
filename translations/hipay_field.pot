# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  hipay_field.admin.inc: n/a
#  hipay_field.affiliates.inc: n/a
#  hipay_field.payments.inc: n/a
#  hipay_field.hipay.inc: n/a
#  hipay_field.module: n/a
#  hipay_field.install: n/a
#  hipay_field.info: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2012-01-15 21:15+0100\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: hipay_field.admin.inc:15
msgid "Should we use the SANDBOX environment."
msgstr ""

#: hipay_field.admin.inc:17
msgid "Activate Sandbox"
msgstr ""

#: hipay_field.admin.inc:22
msgid "Indicate the tax value that should be applied, in percent"
msgstr ""

#: hipay_field.admin.inc:24
msgid "Tax value"
msgstr ""

#: hipay_field.admin.inc:31
msgid "ID of the website registered with Hipay."
msgstr ""

#: hipay_field.admin.inc:33
msgid "Website ID"
msgstr ""

#: hipay_field.admin.inc:42
msgid "Merchant web site password)"
msgstr ""

#: hipay_field.admin.inc:49
msgid "Merchant webservice login"
msgstr ""

#: hipay_field.admin.inc:56
msgid "Merchant webservice password"
msgstr ""

#: hipay_field.admin.inc:65
msgid "Main currency that will be used for each account"
msgstr ""

#: hipay_field.admin.inc:69
msgid "Main currency"
msgstr ""

#: hipay_field.admin.inc:74
msgid "Which accounts will you use?"
msgstr ""

#: hipay_field.admin.inc:77
msgid "Merchant accounts"
msgstr ""

#: hipay_field.admin.inc:82
msgid "Which is the primary account that will be used for payments?"
msgstr ""

#: hipay_field.admin.inc:85
msgid "Primary account"
msgstr ""

#: hipay_field.admin.inc:90
msgid "Which is the secoundary account that will be used as tax account? If none provided, main account will be used."
msgstr ""

#: hipay_field.admin.inc:93
msgid "Tax account"
msgstr ""

#: hipay_field.admin.inc:98
msgid "Which is the secoundary account that will be used as fixed costs account? If none provided, main account will be used."
msgstr ""

#: hipay_field.admin.inc:101
msgid "Fixed costs account"
msgstr ""

#: hipay_field.admin.inc:106
msgid "Which is the secoundary account that will be used as insurance cost accounts? If none provided, main account will be used."
msgstr ""

#: hipay_field.admin.inc:109;117
msgid "Insurances account"
msgstr ""

#: hipay_field.admin.inc:114
msgid "Which is the secoundary account that will be used as shipping costs accounts? If none provided, main account will be used."
msgstr ""

#: hipay_field.affiliates.inc:24 hipay_field.payments.inc:140
msgid "Terms of use of the field"
msgstr ""

#: hipay_field.affiliates.inc:25 hipay_field.payments.inc:141
msgid "You can add terms of use to be accepted. Leave blank if no terms of use."
msgstr ""

#: hipay_field.affiliates.inc:108
msgid "SIGN IN OR CREATE AN HIPAY ACCOUNT"
msgstr ""

#: hipay_field.affiliates.inc:109
msgid "Click on the button and follow the instructions on the Hipay website."
msgstr ""

#: hipay_field.affiliates.inc:111
msgid "VIEW MY HIPAY ACCOUNT"
msgstr ""

#: hipay_field.affiliates.inc:112
msgid "view your Hipay account"
msgstr ""

#: hipay_field.affiliates.inc:115
msgid "INDICATE YOUR HIPAY ACCOUNT INFORMATION FOR YOUR SHOP"
msgstr ""

#: hipay_field.affiliates.inc:116
msgid "You can indicate your Hipay account here to display a \"payment button\" on each products and be able to receive directly payments anonymously.<br> !where"
msgstr ""

#: hipay_field.affiliates.inc:116
msgid "Where can i find necessary information on Hipay website?"
msgstr ""

#: hipay_field.affiliates.inc:117
msgid "Account confirmed and ready to receive payments."
msgstr ""

#: hipay_field.affiliates.inc:128
msgid "Hipay User ID"
msgstr ""

#: hipay_field.affiliates.inc:146
msgid "Hipay Account"
msgstr ""

#: hipay_field.affiliates.inc:159 hipay_field.payments.inc:405
msgid "I accept the terms of use: !tofuse"
msgstr ""

#: hipay_field.affiliates.inc:159 hipay_field.payments.inc:405
msgid "read"
msgstr ""

#: hipay_field.affiliates.inc:250
msgid "You must accept the terms of use to use the payments interface."
msgstr ""

#: hipay_field.affiliates.inc:253
msgid "The user account specified for %field is not valid."
msgstr ""

#: hipay_field.affiliates.inc:257
msgid "The account specified for %field is not valid."
msgstr ""

#: hipay_field.hipay.inc:142 hipay_field.payments.inc:324;326
msgid "Banking and transaction costs"
msgstr ""

#: hipay_field.hipay.inc:154
msgid "!seller's shop"
msgstr ""

#: hipay_field.hipay.inc:434
msgid "Name"
msgstr ""

#: hipay_field.hipay.inc:435
msgid "Address"
msgstr ""

#: hipay_field.hipay.inc:436
msgid "Zip code"
msgstr ""

#: hipay_field.hipay.inc:437 hipay_field.payments.inc:388
msgid "City"
msgstr ""

#: hipay_field.hipay.inc:388;413 hipay_field.payments.inc:483;515;575 hipay_field.module:398
msgid "hipay_field"
msgstr ""

#: hipay_field.hipay.inc:388
msgid "The order referenced !ref purchased by %user (%node) failed."
msgstr ""

#: hipay_field.hipay.inc:413
msgid "Update order status failed for: !ref to status !status by user !user: !mysql_error"
msgstr ""

#: hipay_field.payments.inc:24
msgid "Price field"
msgstr ""

#: hipay_field.payments.inc:27
msgid "The field where the price will be grabbed."
msgstr ""

#: hipay_field.payments.inc:33
msgid "Optional field to hide"
msgstr ""

#: hipay_field.payments.inc:36
msgid "This field will be hidden if purchase button is presented to the user."
msgstr ""

#: hipay_field.payments.inc:41
msgid "Affiliates"
msgstr ""

#: hipay_field.payments.inc:42
msgid "Please chose here the properties that should be applied to each affiliates."
msgstr ""

#: hipay_field.payments.inc:66
msgid "Rate in %"
msgstr ""

#: hipay_field.payments.inc:74
msgid "Account field"
msgstr ""

#: hipay_field.payments.inc:81
msgid "Global commission rate in %"
msgstr ""

#: hipay_field.payments.inc:86
msgid "This amount will be added to cover the transaction costs."
msgstr ""

#: hipay_field.payments.inc:89
msgid "Capture done asap"
msgstr ""

#: hipay_field.payments.inc:91
msgid "After @count days."
msgstr ""

#: hipay_field.payments.inc:95
msgid "Capture day"
msgstr ""

#: hipay_field.payments.inc:98
msgid "How many time to wait between the authorisation and final payment capture."
msgstr ""

#: hipay_field.payments.inc:102
msgid "Available to any public"
msgstr ""

#: hipay_field.payments.inc:104
msgid "Only to a public over @age."
msgstr ""

#: hipay_field.payments.inc:108
msgid "Order rating"
msgstr ""

#: hipay_field.payments.inc:111
msgid "Public rating"
msgstr ""

#: hipay_field.payments.inc:117
msgid "Affiliates Type"
msgstr ""

#: hipay_field.payments.inc:118
msgid "Consider affiliates given price to include (private) or exclude (professional) VAT."
msgstr ""

#: hipay_field.payments.inc:126
msgid "Grabb all amount if no affiliate? (deactivate hipay payment if unchecked)"
msgstr ""

#: hipay_field.payments.inc:132
msgid "Is this product shippable? (will hide requests for shipping informations)"
msgstr ""

#: hipay_field.payments.inc:225
msgid "Buy now!"
msgstr ""

#: hipay_field.payments.inc:281
msgid "<h3>Secured Credit Card purchase via Hipay payment system</h3>\n        <ul>\n            <li>Following the capture of your payment information, your purchase will be on hold.</li>\n            <li>You will be debited as soon as the seller will confirm the order.</li>\n            <li>If this purchase is not confirmed within !capture_days days, the payment will be automatically cancelled and you won't be debited.</li>\n        </ul>"
msgstr ""

#: hipay_field.payments.inc:293
msgid "Purchase on !user's shop"
msgstr ""

#: hipay_field.payments.inc:326
msgid "VAT !tax_value % on !tax_base"
msgstr ""

#: hipay_field.payments.inc:327
msgid "TOTAL"
msgstr ""

#: hipay_field.payments.inc:340
msgid "Shipping information"
msgstr ""

#: hipay_field.payments.inc:341
msgid "Those information are private and will never be used for any communication neither transmitted to partners."
msgstr ""

#: hipay_field.payments.inc:342
msgid "If you want to save those information for later use, please fill in your !account_link."
msgstr ""

#: hipay_field.payments.inc:342
msgid "private profile information"
msgstr ""

#: hipay_field.payments.inc:351
msgid "Last name"
msgstr ""

#: hipay_field.payments.inc:360
msgid "First name"
msgstr ""

#: hipay_field.payments.inc:371;536
msgid "Delivery address"
msgstr ""

#: hipay_field.payments.inc:379
msgid "Zipcode"
msgstr ""

#: hipay_field.payments.inc:398
msgid "Personal message"
msgstr ""

#: hipay_field.payments.inc:399
msgid "You can add a personal message to the seller, some specifications, etc"
msgstr ""

#: hipay_field.payments.inc:413
msgid "Pay your order"
msgstr ""

#: hipay_field.payments.inc:415
msgid "Contacting Hipay. Please Wait..."
msgstr ""

#: hipay_field.payments.inc:436
msgid "You must accept the terms of use to order."
msgstr ""

#: hipay_field.payments.inc:526 hipay_field.module:564
msgid "approve"
msgstr ""

#: hipay_field.payments.inc:527 hipay_field.module:565
msgid "cancel"
msgstr ""

#: hipay_field.payments.inc:532
msgid "Here is the customer's message:"
msgstr ""

#: hipay_field.payments.inc:542
msgid "Hello !seller!\n\n                        !buyer has just bought your speciality !product.\n\n                        You must send your order to him before the !delay. Once you have done it, click on 'approve' to release and receive your payment.\n\n\n                        If you don't accept your order, click on 'cancel'.\n\n\n                        Click here to approve: !approve\n\n                        Click here to refuse: !cancel\n\n\n                        !message\n                        !shipping\n                        Enjoy!"
msgstr ""

#: hipay_field.payments.inc:553
msgid "New Reservation: !node_title"
msgstr ""

#: hipay_field.payments.inc:585
msgid "Thank you for purchasing !product to !user! !user was informed of your order."
msgstr ""

#: hipay_field.payments.inc:587
msgid "You have not been debited yet. The seller must accept your order within !delay days to receive your payment (confirmed to you by email). Otherwise you will automatically be refunded."
msgstr ""

#: hipay_field.payments.inc:592
msgid "Your order has been refused."
msgstr ""

#: hipay_field.payments.inc:594
msgid "Your order is cancelled."
msgstr ""

#: hipay_field.payments.inc:634
msgid "Transaction @ref is: @action. Update will appear shortly on screen."
msgstr ""

#: hipay_field.payments.inc:638
msgid "Error: @message. Your request is being processed by Hipay. Please wait..."
msgstr ""

#: hipay_field.payments.inc:483
msgid "HIPAY ERROR IN RESPONSE"
msgstr ""

#: hipay_field.payments.inc:515
msgid "Success for order: !answer ."
msgstr ""

#: hipay_field.payments.inc:575
msgid "The order response: !answer ."
msgstr ""

#: hipay_field.module:150
msgid "The <a href=\"!module\">%hipay_field</a> module uses the <a href=\"!api\">%api</a> Merchant API to be able to treat payments."
msgstr ""

#: hipay_field.module:168
msgid "Hipay Payments"
msgstr ""

#: hipay_field.module:169
msgid "Create a button to receive payments"
msgstr ""

#: hipay_field.module:177
msgid "Hipay Affiliate"
msgstr ""

#: hipay_field.module:178
msgid "Create a field to capture affiliate account"
msgstr ""

#: hipay_field.module:229
msgid "Hipay Payment Button"
msgstr ""

#: hipay_field.module:235
msgid "Hipay Account Fields"
msgstr ""

#: hipay_field.module:239
msgid "An edit widget to select the account for Hipay."
msgstr ""

#: hipay_field.module:299
msgid "Use standard button"
msgstr ""

#: hipay_field.module:304
msgid "Popup display"
msgstr ""

#: hipay_field.module:309
msgid "Show HiPay Button"
msgstr ""

#: hipay_field.module:314
msgid "Show Affiliate Account"
msgstr ""

#: hipay_field.module:347
msgid "Raw, unfiltered text."
msgstr ""

#: hipay_field.module:348
msgid "Formatted and filtered text."
msgstr ""

#: hipay_field.module:431
msgid "Hipay: payment validation requested"
msgstr ""

#: hipay_field.module:435
msgid "Hipay: ALL payment validation requested (for admins)"
msgstr ""

#: hipay_field.module:439
msgid "Hipay: orders history"
msgstr ""

#: hipay_field.module:443
msgid "Hipay: ALL orders history"
msgstr ""

#: hipay_field.module:472
msgid "Show archive during X days"
msgstr ""

#: hipay_field.module:474
msgid "After this numbers of day, confirmed or cancelled orders will disappear from all blocks."
msgstr ""

#: hipay_field.module:495
msgid "Paid orders pending needing to be confirmed!"
msgstr ""

#: hipay_field.module:501
msgid "Admin: All pending orders"
msgstr ""

#: hipay_field.module:505
msgid "Orders history"
msgstr ""

#: hipay_field.module:509
msgid "Admin: All orders history"
msgstr ""

#: hipay_field.module:579
msgid "your"
msgstr ""

#: hipay_field.module:581
msgid "Someone"
msgstr ""

#: hipay_field.module:583
msgid "!name's"
msgstr ""

#: hipay_field.module:615;621
msgid "inited"
msgstr ""

#: hipay_field.module:616
msgid "authorized"
msgstr ""

#: hipay_field.module:617
msgid "paid and confirmed"
msgstr ""

#: hipay_field.module:618
msgid "cancelled"
msgstr ""

#: hipay_field.module:619
msgid "cancelled and refunded"
msgstr ""

#: hipay_field.module:620
msgid "cancelled and rejected"
msgstr ""

#: hipay_field.module:670
msgid "Hipay Field"
msgstr ""

#: hipay_field.module:398
msgid "The <a href=\"!library\">%hipay</a> Merchant API required by the <a href=\"!module\">%hipay_field</a> module could not be loaded."
msgstr ""

#: hipay_field.module:42
msgid "configure hipay settings"
msgstr ""

#: hipay_field.module:42
msgid "view payment button"
msgstr ""

#: hipay_field.module:42
msgid "can be affiliate"
msgstr ""

#: hipay_field.module:42
msgid "manage capture"
msgstr ""

#: hipay_field.module:42
msgid "manage any capture"
msgstr ""

#: hipay_field.module:53;61;71
msgid "Configure settings for the Hipay Field module."
msgstr ""

#: hipay_field.module:57;65
msgid "Hipay settings"
msgstr ""

#: hipay_field.module:75
msgid "Sandbox"
msgstr ""

#: hipay_field.module:81
msgid "All Hipay orders history."
msgstr ""

#: hipay_field.module:83
msgid "Order History"
msgstr ""

#: hipay_field.module:90
msgid "Callback for the hipay registration page"
msgstr ""

#: hipay_field.module:99;108;117
msgid "Callback for the hipay payment page"
msgstr ""

#: hipay_field.module:126
msgid "Callback to confirm a hipay manual capture"
msgstr ""

#: hipay_field.module:135
msgid "Callback to view cgu of a field in popup"
msgstr ""

#: (duplicate) hipay_field.install:110 
msgid "<a href=\"!simplexml\">%simplexml</a> extension"
msgstr ""

#: (duplicate) hipay_field.install:112 
msgid "The <a href=\"!hipay_field\">%hipay_field</a> module requires the PHP <a href=\"!simplexml\">%simplexml</a> extension."
msgstr ""

#: (duplicate) hipay_field.install:116 
msgid "<a href=\"!curl\">%curl</a> extension"
msgstr ""

#: (duplicate) hipay_field.install:118 
msgid "The <a href=\"!hipay_field\">%hipay_field</a> module requires the PHP <a href=\"!curl\">%curl</a> extension."
msgstr ""

#: (duplicate) hipay_field.install:122 
msgid "<a href=\"!ssl\">%ssl</a> extension"
msgstr ""

#: (duplicate) hipay_field.install:124 
msgid "The <a href=\"!hipay_field\">%hipay_field</a> module requires the PHP <a href=\"!ssl\">%ssl</a> extension."
msgstr ""

#: (duplicate) hipay_field.install:128 
msgid "<a href=\"!help\">%hipay</a> Merchant API"
msgstr ""

#: (duplicate) hipay_field.install:132 
msgid "The <a href=\"!help\">%hipay</a> Merchant API was found."
msgstr ""

#: (duplicate) hipay_field.install:133 
msgid "The <a href=\"!help\">%hipay</a> Merchant API was not found."
msgstr ""

#: hipay_field.info:0
msgid "Hipay field"
msgstr ""

#: hipay_field.info:0
msgid "Allows the user to define computed values in custom content types."
msgstr ""

#: hipay_field.info:0
msgid "CCK"
msgstr ""

