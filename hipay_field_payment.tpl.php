<?php

/**
 * @file
 * Template file for displaying the node content, associated with an image, in
 * the lightbox.  It displays it without any sidebars, etc.
 */
?>
<html>
 <?php print $head ?>
 <?php print $styles ?>
 <?php print $scripts ?>
 <body>
<?php print ($content); ?>
</body>
</html>