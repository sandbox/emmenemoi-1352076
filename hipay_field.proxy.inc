<?php

/*      TO BE INCLUDED IN hook_menu()
      
        $items['hipay_field/hipay_register/%'] = array(
          'access arguments' => array('can be affiliate'),
          'description' => 'Callback for the hipay registration page',
          'page callback' => '_hipay_field_get_hipay',
          'page arguments' => array('proxy',2),
          'file' => 'hipay_field.affiliates.inc',
          'type' => MENU_CALLBACK,
        );

*/

function _hipay_field_get_register($lang='fr',$base='',$proxy_path='/') {
  
   $url = $base.$proxy_path;
   if (extension_loaded('apc')) {
      $cached_var = unserialize(apc_fetch(HIPAY_FIELD_APC));
      if ($cached_var != null)
        $file = $cached_var[md5($url)];
   }
   
   if ($file==null) {
      $opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"Accept-language: ".$lang."\r\n" .
                    "Cookie: foo=bar\r\n"
           )
      );
      
      $context = stream_context_create($opts);
      
      // Open the file using the HTTP headers set above
      $file = file_get_contents($url, false, $context);
      if (extension_loaded('apc')) {
        $cached_var[md5($url)] = $file;
        apc_store(HIPAY_FIELD_APC,serialize($cached_var), 60*60*24);
      }
   }
   $fileCorrected = $file;
   // keep original domain
   //$regex = "/(src\s*=\s*['\"]|href\s*=\s*['\"])([^'\"]+)(['\"][^>]*>)/i";
   //$fileCorrected = preg_replace($regex, "$1https://www.hipay.com$2$3", $file);
   //$fileCorrected = preg_replace("#(<head>)(.*?)(</head>)#is", "$1<base href=\"".$base."\" />$2$3", $file);
   //$fileCorrected = preg_replace("#(<head>)(.*?)(</head>)#is", "$1<base href=\"".url("hipay_field/hipay_register",array('absolute'=>true))."\" />$2$3", $file);
   $base_url = url("hipay_field/hipay_register",array('absolute'=>true));
   $fileCorrected = preg_replace("#(\s(?:href|src)=[\"'])(?!http://)\.?/?([^\"'\r\n]+)#i", "$1$base_url/$2", $fileCorrected);
   
   
   // tune page
   $fileCorrected = preg_replace("#<ul class=\"menu\">.*?</ul>#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#<td>\s*<input id=\"radio_button_pro\".*?</td>#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#<tr>\s*<th\s*>Compte.*?Compra Facil.*?</tr>#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#<!-- Compte de test -->.*?<!-- fin Compte de test -->#is", "", $fileCorrected);
   $fileCorrected = preg_replace("#(<label><input[\w=\s'\"]*hipay_info['\"\s]*?value=\"1\")\s*?(/>[\w\s]*?</label>)#is", "$1 checked $2", $fileCorrected);
   $fileCorrected = preg_replace("#(<label><input[\w=\s'\"]*hipay_part['\"\s]*?value=\"1\")\s*?(/>[\w\s]*?</label>)#is", "$1 checked $2", $fileCorrected);
      
   
   // add necessary informations.
   $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"email1\"\s*id=\"email1\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->mail."$2", $fileCorrected);
   $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"email2\"\s*id=\"email2\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->mail."$2", $fileCorrected);
   $fileCorrected = preg_replace("#(<option\s*value=\"EUR\"\s*label=\"Euro\"\s*)(>\s*Euro\s*</option>)#is", "$1 selected $2", $fileCorrected);

   //dsm($user);
   if (isset($user->field_sex)) {
       $sex = $user->field_sex[0]['value'] == 'M' ? '1' : '2';
       $fileCorrected = preg_replace("#(<select\s*name=\"civilite\".*?<option\s*value=\"".$sex."\" label=[\"\w]*)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
   }
   if (isset($user->field_firstname)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"prenom\"\s*id=\"prenom\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->field_firstname[0]['value']."$2", $fileCorrected);
             
   if (isset($user->field_lastname)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"nom\"\s*id=\"nom\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->field_lastname[0]['value']."$2", $fileCorrected);

   if (isset($user->field_address)) 
        $fileCorrected = preg_replace("#(<textarea\s*name=\"adresse[\s\"\w\d=']*>)\s*(</textarea>)#is", "$1 ".$user->field_address[0]['value']." $2", $fileCorrected);

   if (isset($user->field_zipcode)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"code_postal\"\s*id=\"code_postal\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1 ".$user->field_zipcode[0]['value']." $2", $fileCorrected);

   if (isset($user->field_city)) 
        $fileCorrected = preg_replace("#(<input\s*type=\"text\"\s*name=\"ville\"\s*id=\"ville\"\s*value=\")(\"\s*size=\"[0-9]+\"\s*maxlength=\"[0-9]+\">)#is", "$1".$user->field_city[0]['value']."$2", $fileCorrected);
        
   if (isset($user->field_country)) {
        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"pays\">.*?<option[\s\w\"'=]*?label=\"".strtoupper($user->field_country[0]['value'])."\"\s*)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
   }

   $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"language\">.*?<option[\s\w\"'=]*?value=\"".$lang."_".($lang=='en'?'GB':strtoupper($lang))."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
   $fileCorrected = preg_replace("#(<option\s*value=\"Europe/Paris\"[\s\w\"'=]*?)(.*?</option>)#is", "$1 selected $2", $fileCorrected);
   
   if (isset($user->field_birthday)) {
        $birthday = strtotime($user->field_birthday[0]['value']);
        $birthday_day = date('j',$birthday);
        $birthday_month = date('n',$birthday);
        $birthday_year = date('Y',$birthday);  

        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"Date_Day\">.*?<option[\s\w\"'=]*?value=\"".$birthday_day."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"Date_Month\">.*?<option[\s\w\"'=]*?value=\"".$birthday_month."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
        $fileCorrected = preg_replace("#(<select[\s\w\"'=]*?id=\"Date_Year\">.*?<option[\s\w\"'=]*?value=\"".$birthday_year."\"\s*label=.*?)(>.*?</select>)#is", "$1 selected $2", $fileCorrected);
                        
   }
   //$fileCorrected .= "<script>jQuery(document).ready(function($) { initHipayCatcher(); });</script>";
   //$fileCorrected .= '<br><br>'.$url;
   
   return $fileCorrected; 
}


function _hipay_field_get_proxy($lang='fr',$base='',$proxy_path='/') {
   $GLOBALS['devel_shutdown'] = FALSE;
     /*$opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"Accept-language: ".$lang."\r\n" .
                    "Cookie: foo=bar\r\n"
           )
      );
      
      $context = stream_context_create($opts);
      */
      // Open the file using the HTTP headers set above
      $url = $base.'/'.$proxy_path;
      $url = str_replace(base_path()."hipay_field/hipay_register",$base,$_SERVER['REQUEST_URI']);
      $file = file_get_contents($url, false, $context);
      $headers = get_headers($url);
      header(implode('\n\r', $headers)); 
      return $file;
      return $url;
}